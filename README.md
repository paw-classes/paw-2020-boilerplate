# PAW 2020 API Project Template

This project aims to help PAW students to setup the foundation of a REST API and to help with code organization. Along this documentation you will find information about how to use this boilerplate and also about do it your self.

Index:
 - [Using the boilerplate](#using-this-boilerplate)
 - [Using express generator](#using-express-generator)
 - [Step-by-step](#step-by-step)
    1 - [Create a npm project](#1-create-a-npm-project)
    2 - [Create the source directory and the file to bootstrap the application](#2-create-the-source-directory-and-the-file-to-bootstrap-the-application)
    3 - [Create an express application and run it from you command line](#3-create-an-express-application-and-run-it-from-you-command-line)
    4 - [Setup your dev environment](#4-setup-your-dev-environment)
    5 - [Create the API routes using express router and register them in the express application](#5-create-the-api-routes-using-express-router-and-register-them-in-the-express-application)
    6 - [Connection to Mongo DB](#6-connection-to-mongo-db)
    7 - [Built-in and external middleware that you will need for you REST API](#7-built-in-and-external-middleware-that-you-will-need-for-you-rest-api)
    8 - [Authentication and Authorization strategy](#8-authentication-and-authorization-strategy)
    9 - [Set-up swagger to work with jsdoc](#9-set-up-swagger-to-work-with-jsdoc)

## Using this boilerplate

In order to use this project template, you just need to clone or download the project. Enter the root folder (the one that has the file called package.json) and enter the following commands in the terminal:
```sh
npm install
npm run dev
```

<sup>Note: Read the step-by-step guide in order to understand the code structure.</sup>

## Using express-generator

Run the generator application with the npx command.

``` sh
npx express-generator my-project
```

Jump to [step 4](#4-setup-your-dev-environment) of this documentation and follow along.

## Step-by-Step

Creating a new project from scratch is a simple task. Every time you do it, you learn a better way how to organize your code and how to setup express apps for all your needs.

If yoy fill you want to do it, follow the steps below:

#### 1 - Create a npm project

Create a new folder for your project and start a npm project inside that folder

``` sh
mkdir my-project
cd my-project
npm init -y
```

 You should now have a file called package.json in the root directory. We are now ready to setup our express application.

 Note: You should not forget to add the .gitignore file on your root directory. If you are not sure what to put in the file, you can use this one: https://raw.githubusercontent.com/github/gitignore/master/Node.gitignore

#### 2 - Create the source directory and the file to bootstrap the application

Create a 'src' folder in the root directory - the src folder should contain all your source code files. Inside the src folder, create a file called app.js. This file will bootstrap your application.

``` sh
mkdir src
touch src/app.js
```

#### 3 - Create an express application and run it from you command line

In order to use express framework, you need first to install it.

``` sh
npm i -S express
```

Next, place the hello world code inside the app.js.
``` javascript
const express = require('express')
const app = express()
const PORT = 3000

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.listen(PORT, () =>
    console.log(`Example app listening at http://localhost:${ PORT }`)
)

```
Now run the application using the node command in your terminal.

``` sh
node src/app.js
```

Now open the url http://localhost:3000 and you should see the following page:

![Hello World](./screenshots/hello-world.png "Hello World")

#### 4 - Setup your dev environment

In order to have a better dev experience we should install two more tools and setup a few things.
First of all lets install the nodemon tool. Nodemon will listen for any file change and restart the server automatically.

``` sh
npm i -D nodemon
```

Next we can set a npm script so we can run our application with npm command and using nodemon. Open the package.json file, in the root folder, and add the dev script inside scripts property:

``` json
"scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "dev": "node_modules/.bin/nodemon --watch src src/app.js"
},
```

Now we can run our app using the following command:

``` sh
npm run dev
```

Everything should work as before, but every time you save a file inside src folder, the application will automatically restart.

The finish basic project the setup, we just need one more step. Add the ability to set *ENV* variables. The *ENV* variables are used to setup each environment at deploy time. They are useful both for production and development environments. The easiest way to do it, is to use the **dotenv** module.

First install the npm module.

``` sh
npm i -S dotenv
```

Next you need to create a .env file in the root directory. and add the following content:

``` sh
APP_PORT = 3000
```

And then you need to load the environment variables and use them. The following code demonstrates how to use it in order to set the express app port.

``` javascript
require('dotenv').config() // It must be the first line of code

const express = require('express')
const app = express()
const PORT = process.env.APP_PORT

//...
```

#### 5 - Create the API routes using express router and register them in the express application

Lets start by create a new folder structure for the API and the components that will contain the source code needed for the API to work. Inside the *'/src'* directory you should create the following directory tree:

``` sh
src
  |-api
  |  |-middleware
  |  |  |-authorize.js
  |  |  |-session.js
  |  |-models
  |  |  |-user.js
  |  |  |-labTest.js
  |  |-controllers
  |  |  |-labTestController.js
  |  |  |-userController.js
  |  |  |-sessionController.js
  |  |-routes
  |  |  |-sessionRoutes.js
  |  |  |-labTestRoutes.js
  |  |  |-userRoutes.js
  |  |-index.js
```

The index.js file is the place where you should export the API module (an express router itself), that you will use to register within the express application.

Lets create the content of that file with a router that will respond to the '/' with an API status 'ok'. Remember, because we are building a REST API the responses should always be a JSON object.

``` javascript
const express = require('express')

const apiRouter = express.Router()

apiRouter.get('/', (req, res) => {
    res.json({
        status: 'ok'
    })
})

module.exports = apiRouter
```

Now import the API module, inside the *'app.js'* file using the *require* function register it into the application using the *use* function. See the code below.

```javascript
require('dotenv').config() // It must be the first line of code

const express = require('express')
const api = require('./api')

const app = express()
const port = process.env.APP_PORT

app.use('/api', api)

app.listen(port, () =>
    console.log(`Example app listening at http://localhost:${ port }`)
)
```

Now if you open the url http://localhost:3000, you will see a message saying 'Cannot GET /', that's because we remove the get method from the application that responds to the root path '/' - Express framework is telling you that you don't have any middleware that responds to that path.

So, how do you see the API responses. Well, because we have register the API router under the path *'/api'* you should only see API responses for paths that start with *'/api'*. If you open the url http://localhost:3000/api, you should see the json response with the status 'ok'.

![API Status](./screenshots/api-status.png "API Status")

The url http://localhost:3000/api can be consider a health check - check if the API is up and running.

<sup>Note: If you are wondering why the screenshot shows a formatted JSON, you may need to install the Chrome Extension called JSON Formatter</sup>

##### 5.1 - Organize your code by entity and/or responsibility

###### 5.1.1 - API Routes

All the API routes most be registered within the base API router. You should import the sub routers and register them using the *'.use'* method as shown below.

``` javascript
const userRoutes = require('./routes/userRoutes')
const sessionRoutes = require('./routes/sessionRoutes')
const labTestRoutes = require('./routes/labTestRoutes')

// ...

apiRouter.use(sessionRoutes)
apiRouter.use(userRoutes)
apiRouter.use(labTestRoutes)

```

Don't forget that you should follow the REST API naming and methods conventions.

```
METHOD: POST|GET|PUT|DELETE => PATH: /RESOURCE/ENTITY_ID/ENTITY_PROPERTY|ENTITY_SUB_COLLECTION
```

- Resource names must be plural (as they return a collection - array of resource entities)

- In order to get a specific entity, you should query the resource collection for the specific item /resource/entity_id.

``` javascript
userRouter.get('/users', userController.find)
userRouter.get('/users/:userId', userController.findById)
userRouter.put('/users/:userId/role', userController.findByIdAndUpdateRole)
```

###### 5.1.2 - API Controllers

Controllers are responsible to most of the business logic of the API. They must be register on the API routes and they handle client requests. They also make the connection between the application and the database, through the models.

###### 5.1.3 - API Models

For this specific project, and because the database is Mongo DB, we will be using Mongoose Models. They are responsible to create, read, update and delete entities into/from the respective collection. You can use mongoose validation methods in order to validate the inputs of the requests.

###### 5.1.4 - API Middleware

Use API middleware to deal with global functionalities. As an example, you can use a middleware deserialize the session user from cookies or request headers and set its value in the request object and another to make the authorization of the routes. You can see both examples in the middleware folder.

#### 6 - Connection to Mongo DB

We will be using Mongoose module to communicate with mongo database. You need to install it and them use its *.connect()* method to start the connection to the database.

``` sh
npm i -S mongoose
```

Create a connection using environment variables. Add the following variables to the *.env* file under the root directory.
``` sh
MONGO_DB_HOST = localhost
MONGO_BD_PORT = 27017
MONGO_DB_DATABASE_NAME = labtests
```

And then call the connect method as follows:

``` javascript
const mongoose = require('mongoose')

// ...

// Read values from environment variables
const PORT = process.env.APP_PORT
const MONGO_DB_HOST = process.env.MONGO_DB_HOST
const MONGO_DB_PORT = process.env.MONGO_DB_PORT
const MONGO_DB_DATABASE_NAME = process.env.MONGO_DB_DATABASE_NAME

// Connect to mongo DB
mongoose
    .connect(
        `mongodb://${ MONGO_DB_HOST }:${ MONGO_DB_PORT }/${ MONGO_DB_DATABASE_NAME }`,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,
        }
    )
    .then((mongoose) => {
        console.log('connected to mongo')
    })
    .catch(console.error)

```

When running the code and if everything is ok, you should see the message "connected to mongo" on the terminal window. If not check the errors, also in the terminal.

Alright! Now you can use the mongoose models in order to fetch/set data into your mongo collections. Look up for examples under the folder */src/api/models* and */src/api/controllers*.

#### 7 - Built-in and external middleware that you will need for you REST API

``` javascript
app.use(express.json()) // to support Content-Type: application/json
app.use(express.urlencoded({ extended: false })) // To support Content-Type: application/x-www-form-urlencoded
```

``` sh
npm i -S cors
npm i -S cookie-parser
```

``` javascript
app.use(cookieParser())
// ...
app.use('/api', cors(), api) // enable cors just for the API
```

#### 8 - Authentication and Authorization strategy

For authentication and session management, we will be using JWT token hosted inside a cookie. The mechanism is basic: validate the authenticity of the user, checking username and password, and then use the user information within the signed token to set the cookie value (see *.login()* method in the file */src/api/controllers/sessionController.js*).

``` javascript
const expiration = process.env.SESSION_EXP
const secret = process.env.JWT_SECRET
const jwtToken = jwt.sign(user, secret, { expiresIn: expiration / 1000 })
res.cookie(
    'session',
    jwtToken,
    {
        expires: new Date(Date.now() + expiration),
        httpOnly: true
    }
)
res.json({
    user: {
        ...user,
        password: undefined
    },
    token: jwtToken
})
```

By the way, you need to install jsonwebtoken module and require it.
``` sh
npm i -S jsonwebtoken
```

The token must be validated in all subsequent request. The better way is to create a middleware that validates the token and then sets the sessionUser into the request object, so all the following middleware can check the session using *req.sessionUser*.

``` javascript
const jwt = require('jsonwebtoken')

const secret = process.env.JWT_SECRET || ''
const parseToken = (req) => {
    if (req.cookies.session && req.cookies.session.length) {
        return req.cookies.session
    } else {
        const authorization = req.header('Authorization')
        if (authorization && authorization.startsWith('Bearer ')) {
            return authorization.substring(7)
        }
    }
    return null
}

const sessionMiddleware = (req, res, next) => {
    const sessionStr = parseToken(req)
    try {
        if (sessionStr) {
            const user = jwt.verify(sessionStr, secret)
            req.sessionUser = user
        } else {
            req.sessionUser = null
        }
    } catch (e) {
        console.error(e.message)
        req.sessionUser = null
    }
    next()
}

module.exports = sessionMiddleware
```

Notice that we are reading the token both from the cookie and the *Authorization* header to be compatible with both mechanisms.

#### 9 - Set-up swagger to work with jsdoc

All your API documentation and testing can be done using swagger. There are two modules that you can use to achieve that.
``` sh
npm i -S swagger-jsdoc
npm i -S swagger-ui-express
```

The first one gives you the ability to write JSDoc like documentation for swagger. Check the routes within */src/api/routes/* directory to find a few examples. The second one can be use with express router to serve both the swagger UI and serve json of API documentation.

``` javascript
const express = require('express')
const swaggerUi = require('swagger-ui-express')
const swaggerJSDoc = require('swagger-jsdoc')

const parameters = require('./parameters.json')
const definitions = require('./definitions.json')
const responses = require('./responses.json')

const swaggerRouter = express.Router()
const options = { // line 27
	swaggerDefinition: {
		info: {
			title: 'Lab Tests API', // Title (required)
			version: '1.0.0', // Version (required)
		},
		basePath: '/api',
		parameters,
		definitions,
		responses,
		tags: ['Maintenance', 'Lab Tests', 'User', 'Session']
	},
	apis: [
		'src/api/index.js',
		'src/api/routes/*Routes.js'
	], // Path to the API docs
}
const swaggerSpec = swaggerJSDoc(options)

swaggerRouter.get('/api-docs.json', function (req, res) { // line 41
	res.json(swaggerSpec);
})

swaggerRouter.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec))

module.exports = swaggerRouter
```

If you require and register the new router in the express application, you will be able to open 2 new urls.

One for the swagger ui: http://localhost:3000/api-docs.
![Swagger UI](./screenshots/swagger-ui.png "Swagger UI")

And another for the json file: http://localhost:3000/api-docs.json.
![Swagger API JSON](./screenshots/swagger-json.png "Swagger API JSON")

The End.