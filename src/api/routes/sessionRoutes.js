const express = require('express')
const sessionController = require('../controllers/sessionController')

const sessionRouter = express.Router()

/**
 * @swagger
 * /login:
 *   post:
 *     description: Login to the application
 *     tags: [Session]
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/parameters/username'
 *       - $ref: '#/parameters/password'
 *     responses:
 *       200: 
 *         $ref: '#/responses/LoginSuccess'
 */
sessionRouter.post('/login', sessionController.login)

/**
 * @swagger
 * /me:
 *   get:
 *     description: Returns a specific user
 *     tags: [Session]
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Session User
 *         schema:
 *           type: object
 *           $ref: '#/definitions/SessionUser'
 *       '401':
 *         $ref: '#/responses/Unauthorized'
 */
sessionRouter.get('/me', sessionController.me)

sessionRouter.post('/logout', sessionController.logout)

module.exports = sessionRouter