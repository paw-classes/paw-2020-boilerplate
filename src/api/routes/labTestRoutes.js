const express = require('express')
const labTestController = require('../controllers/labTestController')
const labTestRouter = express.Router()

labTestRouter.post('/lab-tests', labTestController.create)

/**
 * @swagger
 * /lab-tests:
 *   get:
 *     description: Returns a list of lab tests
 *     tags: [Lab Tests]
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: List of tests
 *         schema:
 *           type: array
 *           items:
 *             type: object
 */
labTestRouter.get('/lab-tests', labTestController.find)
labTestRouter.get('/lab-tests/:labTestId', labTestController.findById)
labTestRouter.put('/lab-tests/:labTestId', labTestController.findByIdAndUpdate)
labTestRouter.put('/lab-tests/:labTestId/status', labTestController.findByIdAndUpdateStatus)
labTestRouter.delete('/lab-tests/:labTestId', labTestController.findByIdAndDelete)

module.exports = labTestRouter
