const express = require('express')
const swaggerUi = require('swagger-ui-express')
const swaggerJSDoc = require('swagger-jsdoc')

const parameters = require('./parameters.json')
const definitions = require('./definitions.json')
const responses = require('./responses.json')

const swaggerRouter = express.Router()
const options = { // line 27
	swaggerDefinition: {
		info: {
			title: 'Lab Tests API', // Title (required)
			version: '1.0.0', // Version (required)
		},
		basePath: '/api',
		parameters,
		definitions,
		responses,
		tags: ['Maintenance', 'Lab Tests', 'User', 'Session']
	},
	apis: [
		'src/api/index.js',
		'src/api/routes/*Routes.js'
	], // Path to the API docs
}
const swaggerSpec = swaggerJSDoc(options)

swaggerRouter.get('/api-docs.json', function (req, res) { // line 41
	res.json(swaggerSpec);
})

swaggerRouter.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec))

module.exports = swaggerRouter
