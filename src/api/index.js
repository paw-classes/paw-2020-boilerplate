const express = require('express')

const authorize = require('./middleware/authorize')

const userRoutes = require('./routes/userRoutes')
const sessionRoutes = require('./routes/sessionRoutes')
const labTestRoutes = require('./routes/labTestRoutes')

const apiRouter = express.Router()

/**
 * @swagger
 * /:
 *   get:
 *     description: Returns api status
 *     tags: [Maintenance]
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Health check
 *         schema:
 *           type: object
 *           properties:
 *             status:
 *               type: string
 */
apiRouter.get('/', (req, res) => {
    res.json({
        status: 'ok'
    })
})

apiRouter.use(sessionRoutes)
apiRouter.use(userRoutes)

apiRouter.use(authorize('ADMIN', 'COLLABORATOR'), labTestRoutes)

apiRouter.use(function (err, req, res, next) {
    if (err.name === 'ValidationError') {
        console.error('Mongoose Validation Error: You should send error list to the client')
        res.status(400)
    } else {
        // use the error's status or default to 500
        res.status(err.status || 500);
    }

    // send back json data
    res.send({
        message: err.message
    })
})

module.exports = apiRouter
