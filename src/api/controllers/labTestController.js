const find = (req, res) => {
	res.json([{
		id: 'first-test',
		name: 'First Test',
		status: 'pending'
	}])
}
const notImplemented = (req, res, next) => {
	next({
		message: 'Not Implemented',
		status: 406
	})
}

module.exports = {
	create: notImplemented,
	find: find,
	findById: notImplemented,
	findByIdAndUpdate: notImplemented,
	findByIdAndUpdateStatus: notImplemented,
	findByIdAndDelete: notImplemented,
}