const jwt = require('jsonwebtoken')


const login = (req, res) => {
	const { username, password } = req.body
    const user = {
        username: 'edgar',
        password: 'pass123',
        role: 'ADMIN' // 'COLLABORATOR', 'EXTERNAL'
    }
    // const user = User.find({ username })
    // validate password with cryptojs

    const isValid = user.username === username && user.password === password
    if (isValid) {
		const expiration = process.env.SESSION_EXP
		const secret = process.env.JWT_SECRET
        const jwtToken = jwt.sign(user, secret, { expiresIn: expiration / 1000 })
        res.cookie(
            'session',
            jwtToken,
            {
                expires: new Date(Date.now() + expiration),
                httpOnly: true
            }
        )
        res.json({
            user: {
                ...user,
                password: undefined
            },
            token: jwtToken
        })
    } else {
        res.status(401) // 401 Unauthorized also means unauthenticated
        res.json(null)
    }
}

const me = (req, res) => {
	if (req.sessionUser) {
		res.json(req.sessionUser)
	} else {
		res.status(401)
		res.json(null)
	}
}

const logout = (req, res) => {
    res.clearCookie('session')
	res.json({ success: 'true' })
}

module.exports = {
	login,
	logout,
	me
}