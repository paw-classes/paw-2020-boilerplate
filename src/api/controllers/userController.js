const User = require('../models/user')

const mockData = [
	{
		id: '1',
		username: 'edgar',
		firstName: 'Edgar',
		lastName: 'Esteves',
		email: 'efe@estg.ipp.pt',
		role: 'ADMIN'
	}
]

const find = async (req, res, next) => {
	const users = await User.find().catch(next)
	res.json(users)
}
const findById = (req, res) => {
	const user = mockData.find(({ id }) => id === req.params.userId)
	res.json(user || null)
}

const findByIdAndUpdateRole = async (req, res, next) => {
	const updatedUser = await User
		.findByIdAndUpdate(
			req.params.userId,
			{ role: req.body.role },
			{ new: true, runValidators: true },
		)
		.catch(next)
	if (updatedUser) {
		res.json(updatedUser)
	} else {
		next({
			message: 'User not found',
			status: 404
		})
	}
}

module.exports = {
	find,
	findById,
	findByIdAndUpdateRole
}